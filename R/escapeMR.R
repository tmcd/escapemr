#' @title escapeMR
#' 
#' @description Function to initiate the escapeMR Shiny app 
#' in a browser. 
#'
#' @param appName Name of the Shiny app you want to invoke. 
#' The \code{escapeMR} package may come with multiple Shiny apps, 
#' and this parameter specifies which app to run. If 
#' \code{appName} is NULL, a list of available apps is printed. 
#' 
#' @return NULL
#' 
#' @author Trent McDonald
#' 
#' @examples 
#' escapeMR()
#' escapeMR(NULL)
#' escapeMR("shiny")
#' escapeMR("script") # run the script version
#' 
#' @export 
#' 
escapeMR <- function(appName = "shiny"){
  
  if(appName == "script"){
    ans <- CJSscript()
  } else {
    # locate all the shiny app examples that exist
    validApps <- list.files(system.file("shinyApps", package = "escapeMR"))
    
    validAppsMsg <-
      paste0(
        "Valid Apps are: '",
        paste(validApps, collapse = "', '"),
        "'")
    
    # if an invalid example is given, throw an error
    if (!nzchar(appName) ||
        !(appName %in% validApps)) {
      stop(
        'Please run `escapeMR()` with one of the following valid apps as an argument.\n',
        validAppsMsg,
        call. = FALSE)
    }
    
    # find and launch the app
    appDir <- system.file("shinyApps", appName, package = "escapeMR")
    shiny::runApp(appDir, display.mode = "normal")
  }
  
}