# Chinook Carcass Mark-recapture Analysis Using R


This package facilitates analysis of Chinook salmon carcass mark-recapture data 
using with the super-population modification of a Cormack-Jolly-Seber (CJS) 
model in program R (R Core Team 2013).  WEST Inc. statisticians 
Trent McDonald and Ryan Nielson and others developed this package with 
support from the California Department of Fish and Wildlife and 
the U.S. Fish and Wildlife Service, Sacrameno Office. The primary purpose 
of this package is to make a the user interface 
available to users via a RSiny app that runs locally in a browser. 

A detailed description of the survey protocol and statistical 
analysis methods can be found in Chapter 4 of the *California Central Valley Chinook Salmon Escapement Monitoring Plan* (Bergman et al. 2011).

## Installation

### Prerequisites:

To run analyses envisioned in **escapeMR**,  you need the following: 

1. Program R installed. Download the current version from https://cran.r-project.org/bin/windows/base/. 
2. RStudio.  RStudio is not absolutely necessary, but it is highly 
recommended. Instructions here are tailored to RStudio.  Download the 
current free version from https://www.rstudio.com/products/rstudio/download/. 
3. A copy of the CAMP Escapement MS Access database(s) containing raw carcass mark 
and recapture data.  The mark-recapture data base is named `CAMP_Escapement.mdb`. 
You may also have a database file names `RMIS_Data.mdb` that contains 
coded wire tag information. 
4. Or, instead of the CAMP Escapement database(s), you can use CSV copies of the 
requisite tables containing carcass capture histories, covariates, 
chops on first capture, interval lengths, etc. 

### Package Dependency Installation

**escapeMR** requires a few other packages be installed.
Assuming the computer in question has 
unfettered Internet access, perform the following tasks: 
1. Open RStudio
2. In RStudio's Console window, execute the following command:
```
install.packages(c("shiny", "mra", "DT", "withr", "RODBC", "devtools"), repos="https://cran.cnr.berkeley.edu"). 
```
This command will in turn install a number of other packages due 
to dependencies.  Installation of dependent packages need only 
be performed once, unless an update of the dependencies is necessary.
Check that the installation completed correctly by executing the following 
command: 
```
installed.packages()[c("dplyr", "RODBC", "tidyr", "git2r", "devtools"),"Version"]
```
If installation was successful, this command should return the version 
numbers of all packages. 

### Initial **escapeMR** Installation

The master copy of **escapeMR** resides in Trent McDonald's public GitLab
repository. On machines, with R installed and write privelages to the first 
location in `.libPaths()`, the initial installation can be 
accomplished by the following command: 
```
devtools::install_gitlab("tmcd/escapemr",upgrade = "never")
```

 
### Subsequent **escapeMR** Installs (Updates)

After the initial install, users can update the package by 
executing the following: 
```
library(escapeMR)
update_escapeMR()
```
There is no need to uninstall previous versions of the package. 
One can check that the package is properly installed by executing 
the following command: `packageVersion("escapeMR")`.  This 
command should return the version number of the currently 
installed package and it should match the `X.Y.Z` version 
number of the 
current release on GitLab.com. 


## 32-Bit R

Use of this package with CAMP Escapement databases requires 
that R be run in 32-bit mode.
We require 32-bit R 
if and when this package connects to MS Access, and MS Access is 
a 32-bit program.  If we never connect 
to Access (i.e., you use the CSV method), we do not need 32-bit R.
At the same time, it does not hurt to run 32-bit R for all these 
analyses. 

You can set 32-bit R in Rstudio as follows: 
1. In Rstudio, click *Tools* -> *Global Options*
2. In the left-hand list of objects, click *General*
3. In the right-hand portion of the window, toward the top, click *Change...*
4. In the next window, click the middle radio button associated with 
"Use your machine's default version of R (32-bit)"
5. Click "OK", "OK", and "OK"
6. Close Rstudio entirely
7. Restart Rstudio.  R should now be in 32-bit mode.



## Analysis Steps:

Start the **escapeMR** Shiny app with the following code: 
```
library(escapeMR)
escapeMR("shiny")
```

Start the **escapeMR** non-window script with the following code:
```
library(escapeMR)
escapeMR("script")
```


## Literature Cited

Bergman, J. M., R. M. Nielson, and A. Low. 2011. *California Central Valley Chinook salmon escapement monitoring plan.* Pacific States Marine Fisheries Commission and California Department of Fish and Game.

R Core Team. 2013. *R: A language and environment for statistical computing.* R Foundation for
Statistical Computing. Vienna, Austria. ISBN 3-900051-07-0. URL:  http://www.R-project.org/

