rem Set to working directory where the bat file resides
rem cd "%~dp0" sets the batch file to execute inside the current directory. This is GREAT when calling a batch file from Access VBA

cd "%~dp0"
rem "C:\Program Files\R\R-3.4.1\bin\i386\Rscript.exe" app.r > "shinyLog.txt" 2>&1


rem Keep it, is good but only prints partially the R console. Double >> appends output to shiny.Log
rem "C:\Program Files\R\R-3.4.1\bin\i386\Rscript.exe" app.r > shinyLog.txt 2>&1

rem prints output but no the R console
rem "C:\Program Files\R\R-3.4.1\bin\i386\Rscript.exe" app.r > shinyLog.txt 

rem Calling sink.r and sink.r calling app.r (workaround to print a log...see sink.r and new.log)
rem "C:\Program Files\R\R-3.4.1\bin\i386\Rscript.exe" sink.r 

rem WORKS GREAT and prints the entire Rconsole. 2>&1 is used to print a log file with output and errors together.
rem "C:\Program Files\R\R-3.4.1\bin\i386\Rterm.exe" --no-restore --no-save < app.r > "shiny.out" 2>&1

rem This is really nice as well. Notice the R instead of Rscript or Rterm....it prints the entire R screen
rem"C:\Program Files\R\R-3.4.1\bin\i386\R" CMD BATCH --no-save app.r "shiny.log"

rem Works well but R needs to be on the path (make sure the path to R is under 'path' in system env variables).
R CMD BATCH app.r "shinnnyyy.out"

rem Rscript works well when R is on the path. The log only prints partial output
rem Rscript app.r > shinyRscript.log






 



