dataWarningPopup <- function(dataType,message)
{
  showModal(modalDialog(
    title = dataType,
    message,
    size = 'l',
    easyClose = TRUE
  ))
}

cleanCaptureHistory <- function(captureHistory)
{
  
  colnames(captureHistory) <- gsub("[^[:alnum:]]","",tolower(colnames(captureHistory)))
  occ <- gsub("[^0-9]","",colnames(captureHistory))
  
  #isolate the last set of continuous numbers
  # occ[which(rev(duplicated(rev(occ))))] <- ""
  # contiguous <- diff(as.numeric(occ))
  # contiguous[is.na(contiguous)] <- 0
  # occ[1:max(which(contiguous != 1))] <- ""
  
  included <- c("disctag") %in% colnames(captureHistory)
  if(!included)
  {
    dataWarningPopup("CaptureHistory error",
                     "CaptureHistory is Missing disctag column (punctuation and capitalization is ignored)")
    return(NULL)
  }
  
  occ[1:(which(colnames(captureHistory)=="disctag")-1)] <- ""
  occColNum <- which(occ != "")
  colnames(captureHistory)[occColNum] <- occ[occColNum]

  captureHistoryValues <- unique(as.integer(unlist(captureHistory[,occColNum])))
  included <-  captureHistoryValues%in% c(0,1,2)
  if(any(!included))
  {
    dataWarningPopup("CaptureHistory error",
                     "CaptureHistory occasions should only contain 0, 1 or 2")
    return(NULL)
  }
  
  # remove any all zero rows
  zeros <- as.matrix(captureHistory[,occColNum])
  zeros <- rowSums( zeros >= 1, na.rm = TRUE)
  zeros <- which(zeros == 0)
  if( length(zeros) >= 1){
    captureHistory <- captureHistory[-zeros,]
  }
  
  #fill in missing columns
  if(length(occColNum)>0)
  {
    last <- max(as.integer(occ[occColNum]))
    captureHistory0 <- as.data.frame(matrix(0,ncol = 1+last,nrow = nrow(captureHistory)))
    colnames(captureHistory0) <- c("disctag",as.character(1:last))
    
    captureHistory0[,c("disctag",occ[occColNum])] <- captureHistory[,c("disctag",occ[occColNum])]
    captureHistory <- captureHistory0
    return(captureHistory)
  }
  return(captureHistory[,c("disctag",occ[occColNum])])
}

cleanCovariates <- function(covariate,captureHistory)
{
  if(nrow(covariate) != nrow(captureHistory))
  {
    write.csv(covariate, file="tmpCovariate.csv")
    write.csv(captureHistory, file="tmpCaptureHistory.csv")
    print(getwd())
    
    dataWarningPopup("Covariate error",
                     paste("Covariate table row count:",nrow(covariate),"does not match CaptureHistory:",nrow(captureHistory),
                      "and will not be included in analysis"))
    return(NULL)
  }
  covariateFinal <- covariate
  colnames(covariateFinal) <- gsub("[^[:alnum:]]","",tolower(colnames(covariate)))
  
  included <- c("disctag","sex","length") %in% colnames(covariateFinal)
  if(sum(included) != 3)
  {
    dataWarningPopup("Covariate error",
                     paste("Covariate table is missing",c("disctag","sex","length")[!included],
                     "and will not be included in analysis"))
    return(NULL)
  }
  covariateFinal <- covariateFinal[,c("disctag","sex","length")]
   
  # covarFit <- na.omit(covariateFinal[,c("sex","length")])
  # covarFit <- covarFit[ !(covarFit$sex == "N/R" | covarFit$length == "N/R") ,]
  # covarFit$sex <- covarFit$sex == "F"
  # covarFit$length <- as.numeric(covarFit$lengt)
  # test <- glm (sex ~ length, data = covarFit, family = binomial(link = "logit"))
  
  sexCount <- table(covariateFinal$sex)
  if(length(sexCount)>2)
  {
    sexReplacement <- sample(size=sum(!(covariateFinal$sex %in% c("F","M"))),x=c("F","M"), prob = c(as.numeric(sexCount["F"]/sexCount["M"]),1),replace = T)
    covariateFinal$sex[!(covariateFinal$sex %in% c("F","M"))] <- sexReplacement
  }
  covariateFinal$length <- as.numeric(covariateFinal$length)
  
  # Replace 0, missing, and extreme outlier lengths with random normal values
  lengthMean <- mean(covariateFinal$length, na.rm=TRUE)
  lengthSd <- sd(covariateFinal$length, na.rm = TRUE)
  badValIndex <- (covariateFinal$length == 0) | is.na(covariateFinal$length) 
  
  
  maleLengthReplaceIndex <- covariateFinal$sex=="M" & !badValIndex
  maleLengthMean <- mean(covariateFinal$length[maleLengthReplaceIndex])
  maleLengthSd <- sd(covariateFinal$length[maleLengthReplaceIndex])
  maleLengthReplaceIndex <- covariateFinal$sex=="M" & 
    (badValIndex | 
    (covariateFinal$length > (maleLengthMean + 10*maleLengthSd)))
  
  if(sum(maleLengthReplaceIndex)>0)
  {
    warning(paste(sum(maleLengthReplaceIndex),
                  "MALE length value(s) in covariate",
                  "table are either 0, missing, or greater than",
                  "10 sd's from the mean length.\n",
                  "Replacing these with the mean MALE length of",
                  round(maleLengthMean,0)))
    
    covariateFinal$length[maleLengthReplaceIndex] <- round(maleLengthMean,0) 
  }
  
  femaleLengthReplaceIndex <- covariateFinal$sex=="F" & !badValIndex
  femaleLengthMean <- mean(covariateFinal$length[femaleLengthReplaceIndex])
  femaleLengthSd <- sd(covariateFinal$length[femaleLengthReplaceIndex])
  femaleLengthReplaceIndex <- covariateFinal$sex=="F" & 
    (badValIndex | 
    (covariateFinal$length > (femaleLengthMean + 10*femaleLengthSd)))
  
  if(sum(femaleLengthReplaceIndex)>0)
  {
    warning(paste(sum(femaleLengthReplaceIndex),
                  "FEMALE length value(s) in covariate",
                  "table are either 0, missing, or greater than",
                  "10 sd's from the mean length.\n",
                  "Replacing these with the mean FEMALE length of",
                  round(femaleLengthMean,0)))
    
    covariateFinal$length[femaleLengthReplaceIndex] <- round(femaleLengthMean,0) 
  }

  return(covariateFinal)
}

# ---- 

cleanCHOPS <- function(CHOPS,captureHistory)
{
  colnames(CHOPS) <- gsub("[^[:alnum:]]","",tolower(colnames(CHOPS)))
  colnames(CHOPS) <- as.character(c(CHOPS[1],gsub("[^0-9]","",colnames(CHOPS[-1]))))
  
  #if(length(CHOPS) != ncol(captureHistory) | any(!(colnames(CHOPS[-1]) %in% colnames(captureHistory[-1]))))
  if(ncol(captureHistory) > length(CHOPS))
  {
    dataWarningPopup("CHOPS error",
                     paste("CHOPS length:",length(CHOPS),"does not cover CaptureHistory:",ncol(captureHistory),
                           "and will not be included in analysis"))
    return(NULL)
  }
  CHOPS <- CHOPS[as.character(c(colnames(CHOPS)[1],colnames(captureHistory[-1])))]
  print(paste("CHOPS allignment:",try(which.max(CHOPS[-1])),which.max(colSums(captureHistory[,-1]))))
  if(length(CHOPS) != ncol(captureHistory))
  {
    dataWarningPopup("CHOPS error",
                     paste("CHOPS length:",length(CHOPS),"does not match CaptureHistory:",ncol(captureHistory),
                     "and will not be included in analysis"))
    return(NULL)
  }
  return(CHOPS)
}


cleanEscSummary <- function(EscSummary)
{
  # simplify names
  colnames(EscSummary) <- gsub("[^[:alnum:]]","",colnames(EscSummary))
  return(EscSummary)
}
cleanReleaseSummary <- function(ReleaseSummary)
{
  # simplify names
  colnames(ReleaseSummary) <- gsub("[^[:alnum:]]","",colnames(ReleaseSummary))
  naIndex <- is.na(ReleaseSummary$TotalCWT) | is.na(ReleaseSummary$TotalReleased)
  return(ReleaseSummary[!naIndex,])
}
