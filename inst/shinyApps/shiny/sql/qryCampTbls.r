
qryCJStbls <- function(surveyMeta,sqlFile,ch){
  
  try({
    
    print("clearing tempCJS_Covariate")
    sqlQuery(ch,paste0("DROP TABLE tempCJS_Covariate"))
    print("clearing tempCJS_Selections")
    sqlQuery(ch,paste0("DROP TABLE tempCJS_Selections"))
    print("clearing tempCJS_CaptureHistoryPrelim")
    sqlQuery(ch,paste0("DROP TABLE tempCJS_CaptureHistoryPrelim"))
    print("clearing tempCJS_CaptureHistory")
    sqlQuery(ch,paste0("DROP TABLE tempCJS_CaptureHistory"))
    print("clearing tempCJS_ChopsMatrixPrelim")
    sqlQuery(ch,paste0("DROP TABLE tempCJS_ChopsMatrixPrelim"))
    print("clearing tempCJS_ChopsMatrix")
    sqlQuery(ch,paste0("DROP TABLE tempCJS_ChopsMatrix"))
    
  })
  
  # ---- Run the series of queries.
  resultStatus <- F.run.sqlFile( ch, sqlFile, echo=F, check.drops=T)
  if(length(resultStatus)==0)
  {
    # ---- Fetch the resulting tables.  
    print("fetching CJS")
    capture <- sqlFetch(ch, "tempCJS_CaptureHistory")
    chops <- sqlFetch(ch, "tempCJS_ChopsMatrix")
    covariate <- sqlFetch(ch, "tempCJS_Covariate")
    
    # ---- Modify to match expected format
    
    chopsFinal <- chops[which(chops[,1]==surveyMeta),]
    subset <- which(!is.na(chopsFinal))
    chopsFinal <- chopsFinal[,min(subset):max(subset)]
    chopsFinal[is.na(chopsFinal)] <- 0
    

    # ---- Clean
    captureFinal <- cleanCaptureHistory(capture)
    chopsFinal <- cleanCHOPS(chopsFinal,captureFinal)
    covariateFinal <- cleanCovariates(covariate, captureFinal)
    
    CJS <- list(capture=captureFinal,chops=chopsFinal,covariate=covariateFinal)
    return(CJS)
  }else{
    return(resultStatus)
  }
}


qryRMIStbls <- function(sqlFile,ch,estimCJS){
  
  try({
    sqlQuery(ch,paste0("DROP TABLE tempRMIS_EscPrelim"))
    print("clearing tempRMIS_EscPrelim")
    sqlQuery(ch,paste0("DROP TABLE tempRMISReleaseLocationsCat"))
    print("clearing tempRMISReleaseLocationsCat")
    sqlQuery(ch,paste0("DROP TABLE tempRMISRecoveryList"))
    print("clearing tempRMISRecoveryList")
    sqlQuery(ch,paste0("DROP TABLE tempRMISRecoveryCounts"))
    print("clearing tempRMISRecoveryCounts")
    sqlQuery(ch,paste0("DROP TABLE tempRMISRecoveryDetail"))
    print("clearing tempRMISRecoveryDetail")
    sqlQuery(ch,paste0("DROP TABLE tempRMISReleaseSummary"))
    print("clearing tempRMISReleaseSummary")
    sqlQuery(ch,paste0("DROP TABLE tempRMISRecovery"))
    print("clearing tempRMISRecovery")
    sqlQuery(ch,paste0("DROP TABLE tempRMISReleasePrelim"))
    print("clearing tempRMISReleasePrelim")
  })
  
  # ---- Run the series of queries.
  names(estimCJS) <- "[enter escapement from CJS]"
  resultStatus <- F.run.sqlFile( ch, sqlFile, echo=F, check.drops=T, estimCJS)
  
  if(length(resultStatus)==0)
  {
    # ---- Fetch the resulting tables.  
    print("fetching RMIS")
    
    release <- sqlFetch(ch, "tempRMISReleaseSummary")
    recovery <- sqlFetch(ch, "tempRMIS_EscSummary")
  
    # ---- Modify to match expected format
    
    RMIS <- list(release=release,recovery=recovery)
    return(RMIS)
  }else{
    return(resultStatus)
  }
}