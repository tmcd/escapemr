CAMP Escapement RMIS sql from C. Shannon 12/4/2017 This version was created 12/4/2017.


Species code "161980= Chinook.   

SQL below will develop the following finished tables:

temp_RMISEscSummary
temp_RMISRecovery
temp_RMISReleasePrelim
temp_RMISReleaseSummary

Please output tables to the platform directory 'TempAnalysis'

#####################################################

'Build new tables

'1 Make temp recovery table

SELECT 0 AS ConcatID, IIf(luCondition!ConditionID=1, 'fresh', IIf(luCondition!ConditionID=2 Or luCondition!ConditionID=3, 'non-fresh', 'not recorded')) AS Condition, CarcassIndividual.SurveyMetaID, CarcassIndividual.IndividualID, CarcassSurvey.SurveyDate, Location.Location, LocationSection.SectionCD, SurveyMetadata.ActiveSelection, CarcassIndividual.SpeciesID, luSpecies.CommonName, CarcassIndividual.RunID, luRun.run AS Run, CarcassIndividual.CWTcd, CarcassIndividual.HeadNu INTO tempRMISRecoveryDetail FROM (luSpecies INNER JOIN (LocationSection RIGHT JOIN ((CarcassSurvey LEFT JOIN Location ON CarcassSurvey.LocationID=Location.LocationID) INNER JOIN (luRun RIGHT JOIN (luCondition RIGHT JOIN CarcassIndividual ON luCondition.ConditionID=CarcassIndividual.ConditionID) ON luRun.runID=CarcassIndividual.RunID) ON (CarcassSurvey.SurveyID=CarcassIndividual.SurveyID) AND (CarcassSurvey.SurveyMetaID=CarcassIndividual.SurveyMetaID)) ON (LocationSection.SectionID=CarcassSurvey.SectionID) AND (LocationSection.LocationID = CarcassSurvey.LocationID)) ON luSpecies.TaxonID=CarcassIndividual.SpeciesID) INNER JOIN SurveyMetadata ON (SurveyMetadata.SurveyMetaID=CarcassSurvey.SurveyMetaID) AND (luSpecies.TaxonID=SurveyMetadata.TargetSpeciesID) WHERE (((SurveyMetadata.ActiveSelection)=Yes) AND ((CarcassIndividual.CWTcd) Is Not Null));

'2 Add preceeding zero to any CWTs if missing

UPDATE tempRMISRecoveryDetail SET tempRMISRecoveryDetail.CWTcd = '0' & tempRMISRecoveryDetail!CWTcd WHERE (((tempRMISRecoveryDetail.CWTcd) Not Like '0*'));

''3 Make table to recieve release data tempRMISReleasePrelim based on template table (updated the template table to include two new fields, TagLossRate as number double, and TagLossSmpleSize as long integer 9/21/2017)

SELECT tempRMISReleasePrelimTemplate.* INTO tempRMISReleasePrelim FROM tempRMISReleasePrelimTemplate;

''4 Append release records (modified to include Tag loss rate and tag loss sample size 9/21/2017)

INSERT INTO tempRMISReleasePrelim ( TaxonID, Origin, Brood_year, Stock, RunID, Run_Type, CWT_codes, TotalReleased, TotalCWT, TagLossRate, TagLossSmpleSize, Release_Strategy, TotalRecovered, ReleaseLoc ) SELECT luSpecies.TaxonID, IIf(RMISRelease!hatchery_location_code Is Not Null, luRMIS_RelOriginCodesInUse!CampRelORIGINcd, luRMIS_RelOriginCodesInUse_1!CampRelORIGINcd) AS Origin, RMISRelease.brood_year AS Brood_year, IIf(RMISRelease!stock_location_code Is Null, 'Unspecified', luRMIS_StockLocationsInUse!CAMPLocation) AS Stock, RMISRelease.CAMP_runID AS RunID, luRun.run AS Run_Type, RMISRelease.tag_code AS CWT_codes, CLng(RMISRelease!TotalReleased) AS TotalReleased, RMISRelease!TotalCWTed AS TotalCWT, RMISRelease.tag_loss_rate AS TagLossRate, IIf(RMISRelease!tag_loss_rate Is Null, Null, (RMISRelease!tag_loss_sample_size)) AS TagLossSmpleSize, luRMIS_RelLocationsInUse.CAMPLocation AS Release_Strategy, 0 AS TotalRecovered, StrConv(luRMIS_RelLocationsInUse!name, 3) AS ReleaseLoc FROM SurveyMetadata INNER JOIN ((((((RMISRelease LEFT JOIN luRMIS_RelOriginCodesInUse ON RMISRelease.hatchery_location_code = luRMIS_RelOriginCodesInUse.location_code) LEFT JOIN luRMIS_RelOriginCodesInUse AS luRMIS_RelOriginCodesInUse_1 ON RMISRelease.stock_location_code = luRMIS_RelOriginCodesInUse_1.location_code) LEFT JOIN luSpecies ON RMISRelease.CAMP_TaxonID = luSpecies.TaxonID) LEFT JOIN luRun ON RMISRelease.CAMP_runID = luRun.runID) LEFT JOIN luRMIS_StockLocationsInUse ON RMISRelease.stock_location_code = luRMIS_StockLocationsInUse.location_code) LEFT JOIN luRMIS_RelLocationsInUse ON RMISRelease.release_location_code = luRMIS_RelLocationsInUse.location_code) ON SurveyMetadata.TargetSpeciesID = RMISRelease.CAMP_TaxonID WHERE (((RMISRelease.brood_year) Between (Year([SurveyMetadata]![StartDate])-2) And (Year([SurveyMetadata]![StartDate])-4)) AND ((SurveyMetadata.ActiveSelection)=Yes)) ORDER BY IIf(RMISRelease!hatchery_location_code Is Not Null, luRMIS_RelOriginCodesInUse!CampRelORIGINcd, luRMIS_RelOriginCodesInUse_1!CampRelORIGINcd), RMISRelease.brood_year, luRun.run, luRMIS_RelLocationsInUse.CAMPLocation;

'4b Build and group recoveries by CWT

SELECT tempRMISRecoveryDetail.CWTcd, Count(tempRMISRecoveryDetail.IndividualID) AS CountOfIndividualID INTO tempRMISRecoveryCounts FROM tempRMISRecoveryDetail GROUP BY tempRMISRecoveryDetail.CWTcd;

'4c Update the preliminary table with the total recovered
 
UPDATE tempRMISReleasePrelim INNER JOIN tempRMISRecoveryCounts ON tempRMISReleasePrelim.CWT_codes = tempRMISRecoveryCounts.CWTcd SET tempRMISReleasePrelim.TotalRecovered = tempRMISRecoveryCounts!CountOfIndividualID;

'5 Compare the location released with the location of origin. Update Release strategy to Basin where appropriate.

UPDATE tempRMISReleasePrelim INNER JOIN luRMIS_RelLocationsInUse ON tempRMISReleasePrelim.Release_Strategy = luRMIS_RelLocationsInUse.CAMPLocation SET tempRMISReleasePrelim.Release_Strategy = 'Basin' WHERE (((Left([tempRMISReleasePrelim]![Origin], 3))=[luRMIS_RelLocationsInUse]![CAMPLocAdjustment]));

''6 Create tempRMISReleaseSummary from template (updated the template table to include two new fields, TagLossRate as number double, and TagLossSmpleSize as long integer 9/21/2017)

SELECT tempRMISReleaseSummaryTemplate.* INTO tempRMISReleaseSummary FROM tempRMISReleaseSummaryTemplate;

''7 Append grouped release records to final summary (Modified to average two new fields TagLossRate and TagLossSmpleSize 9/21/2017)

INSERT INTO tempRMISReleaseSummary ( TaxonID, Brood_year, Origin, Stock, RunID, Run_Type, CWT_codes, TotalReleased, TotalCWT, Percent_CWT, TagLossRate, TagLossSmpleSize, Release_Strategy, TotalRecovered ) SELECT tempRMISReleasePrelim.TaxonID, tempRMISReleasePrelim.Brood_year, tempRMISReleasePrelim.Origin, tempRMISReleasePrelim.Stock, tempRMISReleasePrelim.RunID, tempRMISReleasePrelim.Run_Type, Count(tempRMISReleasePrelim.CWT_codes) AS CWT_codes, Sum(tempRMISReleasePrelim.TotalReleased) AS TotalReleased, Sum(tempRMISReleasePrelim.TotalCWT) AS TotalCWT, 0 AS Percent_CWT, Avg(tempRMISReleasePrelim.TagLossRate) AS AvgOfTagLossRate, Avg(tempRMISReleasePrelim.TagLossSmpleSize) AS AvgOfTagLossSmpleSize, tempRMISReleasePrelim.Release_Strategy, Sum(tempRMISReleasePrelim.TotalRecovered) AS SumOfTotalRecovered FROM tempRMISReleasePrelim GROUP BY tempRMISReleasePrelim.TaxonID, tempRMISReleasePrelim.Brood_year, tempRMISReleasePrelim.Origin, tempRMISReleasePrelim.Stock, tempRMISReleasePrelim.RunID, tempRMISReleasePrelim.Run_Type, 0, tempRMISReleasePrelim.Release_Strategy ORDER BY tempRMISReleasePrelim.TaxonID, tempRMISReleasePrelim.Brood_year, tempRMISReleasePrelim.Origin, tempRMISReleasePrelim.Stock, tempRMISReleasePrelim.RunID, tempRMISReleasePrelim.Release_Strategy;

'7a Use the ConcateID developed in the final summary table to reverse populate prelim table

UPDATE tempRMISReleaseSummary INNER JOIN tempRMISReleasePrelim ON (tempRMISReleaseSummary.Release_Strategy = tempRMISReleasePrelim.Release_Strategy) AND (tempRMISReleaseSummary.RunID = tempRMISReleasePrelim.RunID) AND (tempRMISReleaseSummary.Stock = tempRMISReleasePrelim.Stock) AND (tempRMISReleaseSummary.Brood_year = tempRMISReleasePrelim.Brood_year) AND (tempRMISReleaseSummary.Origin = tempRMISReleasePrelim.Origin) AND (tempRMISReleaseSummary.TaxonID = tempRMISReleasePrelim.TaxonID) SET tempRMISReleasePrelim.ConcateID = tempRMISReleaseSummary!ConcatID;

'7b Use the ConcateID to reverse populate the recovery detail

UPDATE tempRMISReleasePrelim INNER JOIN tempRMISRecoveryDetail ON tempRMISReleasePrelim.CWT_codes = tempRMISRecoveryDetail.CWTcd SET tempRMISRecoveryDetail.ConcatID = tempRMISReleasePrelim!ConcateID;

'7c Build interim table with cwts by group
 
SELECT tempRMISRecoveryDetail.ConcatID, tempRMISRecoveryDetail.CWTcd, Count(tempRMISRecoveryDetail.IndividualID) AS TotalRecovered INTO tempRMISRecoveryList FROM tempRMISRecoveryDetail GROUP BY tempRMISRecoveryDetail.ConcatID, tempRMISRecoveryDetail.CWTcd HAVING (((tempRMISRecoveryDetail.ConcatID)>0));

'8 Update the percentCWT field

UPDATE tempRMISReleaseSummary SET tempRMISReleaseSummary.Percent_CWT = IIf(tempRMISReleaseSummary!TotalReleased=0 Or tempRMISReleaseSummary!TotalCWT=0, 0, tempRMISReleaseSummary!TotalCWT*100/tempRMISReleaseSummary!TotalReleased);

'10 Prepare to Concatinate detailed release location to summary table. Make tempRMISReleaseLocationsCat

SELECT tempRMISReleasePrelim.ConcateID, tempRMISReleasePrelim.ReleaseLoc INTO tempRMISReleaseLocationsCat FROM tempRMISReleasePrelim GROUP BY tempRMISReleasePrelim.ConcateID, tempRMISReleasePrelim.ReleaseLoc;

'11 Update final summary with detailed release location
'Commented out due to use of "ConcatRelated". Result not needed in app

''UPDATE tempRMISReleaseLocationsCat INNER JOIN tempRMISReleaseSummary ON tempRMISReleaseLocationsCat.ConcateID = tempRMISReleaseSummary.ConcatID SET tempRMISReleaseSummary.ReleaseLoc = ConcatRelated('ReleaseLoc', 'tempRMISReleaseLocationsCat', '[tempRMISReleaseLocationsCat]![ConcateID] = ' & tempRMISReleaseSummary!ConcatID);

'12 Update the recovery table with ConcatIDs from release preliminary
 
UPDATE tempRMISReleasePrelim INNER JOIN tempRMISRecoveryDetail ON tempRMISReleasePrelim.CWT_codes = tempRMISRecoveryDetail.CWTcd SET tempRMISRecoveryDetail.ConcatID = tempRMISReleasePrelim!ConcateID;

'Analysis 1 build tempRMIS_EscPrelim

SELECT SurveyMetadata.SurveyYear, CarcassSurvey.SurveyMetaID, Location.LocationCD, Location.Location, CarcassSurvey.SurveyWeek, 'Individual' AS [Table], CarcassIndividual.IndividualID AS RecordID, luRun.run AS Run, luAdFinStatus.AdFinClip, IIf(CarcassIndividual!HeadNu Is Not Null, 'yes', 'no') AS HeadTaken, IIf(CarcassIndividual!ConditionID=1, 'fresh', IIf(CarcassIndividual!ConditionID=2 Or CarcassIndividual!ConditionID=3, 'non-fresh', 'not recorded')) AS Condition, luSex.Sex, CarcassIndividual.FLmm AS Length, '' AS SizeClass, 1 AS [Count], CarcassIndividual.CWTcd INTO tempRMIS_EscPrelim FROM luSpecies INNER JOIN ((luSubsample RIGHT JOIN (CarcassSurvey LEFT JOIN Location ON CarcassSurvey.LocationID = Location.LocationID) ON luSubsample.SubsampleID = CarcassSurvey.SubsampleID) INNER JOIN ((luRun INNER JOIN (luDisposition RIGHT JOIN (luAdFinStatus RIGHT JOIN (luSex RIGHT JOIN (luCondition RIGHT JOIN CarcassIndividual ON luCondition.ConditionID = CarcassIndividual.ConditionID) ON luSex.SexID = CarcassIndividual.SexID) ON luAdFinStatus.AdFinClipID = CarcassIndividual.AdFinClipID) ON luDisposition.DispositionID = CarcassIndividual.DispositionID) ON luRun.runID = CarcassIndividual.RunID) INNER JOIN SurveyMetadata ON luRun.runID = SurveyMetadata.TargetRunID) ON (SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID) AND (CarcassSurvey.SurveyID = CarcassIndividual.SurveyID) AND (CarcassSurvey.SurveyMetaID = CarcassIndividual.SurveyMetaID)) ON (luSpecies.TaxonID = SurveyMetadata.TargetSpeciesID) AND (luSpecies.TaxonID = CarcassIndividual.SpeciesID) WHERE (((SurveyMetadata.ActiveSelection)=Yes));

'Analysis 2 append chops records

INSERT INTO tempRMIS_EscPrelim ( SurveyYear, SurveyMetaID, LocationCD, Location, SurveyWeek, [Table], RecordID, run, HeadTaken, AdFinClip, Condition, Sex, SizeClass, [Count] ) SELECT SurveyMetadata.SurveyYear, CarcassChops.SurveyMetaID, Location.LocationCD, Location.Location, CarcassSurvey.SurveyWeek, 'Chops' AS [Table], CarcassChops.ChopsID AS RecordID, luRun.run, 'No' AS HeadTaken, luAdFinStatus.AdFinClip, IIf(luChopType!ConditionID=1, 'fresh', IIf(luChopType!ConditionID=2 Or luChopType!ConditionID=3, 'non-fresh', 'not recorded')) AS Condition, luSex.Sex, luSizeClass.SizeClass, CarcassChops.ChopCount FROM (luSpecies INNER JOIN ((luSex RIGHT JOIN (luAdFinStatus RIGHT JOIN (luCondition RIGHT JOIN (luSizeClass RIGHT JOIN luChopType ON luSizeClass.SizeClassID = luChopType.SizeClassID) ON luCondition.ConditionID = luChopType.ConditionID) ON luAdFinStatus.AdFinClipID = luChopType.AdFinClipID) ON luSex.SexID = luChopType.SexID) RIGHT JOIN ((luSubsample RIGHT JOIN (CarcassSurvey LEFT JOIN Location ON CarcassSurvey.LocationID = Location.LocationID) ON luSubsample.SubsampleID = CarcassSurvey.SubsampleID) INNER JOIN (luRun RIGHT JOIN CarcassChops ON luRun.runID = CarcassChops.RunID) ON (CarcassSurvey.SurveyID = CarcassChops.SurveyID) AND (CarcassSurvey.SurveyMetaID = CarcassChops.SurveyMetaID)) ON luChopType.ChopTypeCD = CarcassChops.ChopTypeCD) ON luSpecies.TaxonID = CarcassChops.SpeciesID) INNER JOIN SurveyMetadata ON (SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID) AND (luSpecies.TaxonID = SurveyMetadata.TargetSpeciesID) WHERE (((CarcassChops.ChopCount)>0) AND ((SurveyMetadata.ActiveSelection)=Yes));
 
'Analysis 3 Add preceeding zeros to CWTcodes where missing
 
UPDATE tempRMIS_EscPrelim SET tempRMIS_EscPrelim.CWTcd = '0' & tempRMIS_EscPrelim!CWTcd WHERE (((tempRMIS_EscPrelim.CWTcd) Not Like '0*'));

'Analysis 4 Delete old records from tempRMIS_EscSummary

DELETE DISTINCTROW tempRMIS_EscSummary.*, tempRMIS_EscSummary.SurveyMetaID FROM tempRMIS_EscSummary WHERE (((tempRMIS_EscSummary.SurveyMetaID) Is Not Null));

'Analysis 5 Append new summary records

INSERT INTO tempRMIS_EscSummary ( SurveyYear, SurveyMetaID, LocationCD, Location, Condition, Escapement, ChinookSampled, [ObservedAd-clips], [Ad-ClipUnknown], HeadProcessed, ValidCWTs, SampleRate, AdClipsProcessed, ValidCWT, CWTSampleExpansion ) SELECT tempRMIS_EscPrelim.SurveyYear, tempRMIS_EscPrelim.SurveyMetaID, tempRMIS_EscPrelim.LocationCD, tempRMIS_EscPrelim.Location, tempRMIS_EscPrelim.Condition, [enter escapement from CJS] AS Escapement, Sum(tempRMIS_EscPrelim!Count) AS ChinookSampled, Sum(IIf(tempRMIS_EscPrelim!AdFinClip='Yes', tempRMIS_EscPrelim!Count, IIf(tempRMIS_EscPrelim!AdFinClip='Partial', tempRMIS_EscPrelim!Count, 0))) AS [ObservedAd-clips], Sum(IIf(tempRMIS_EscPrelim!AdFinClip='Unknown', tempRMIS_EscPrelim!Count, 0)) AS [Ad-ClipUnknown], Sum(IIf(tempRMIS_EscPrelim!HeadTaken='yes', tempRMIS_EscPrelim!Count, 0)) AS HeadProcessed, Sum(IIf(tempRMISReleasePrelim!ConcateID Is Not Null, tempRMIS_EscPrelim!Count, 0)) AS ValidCWTs, 0 AS SampleRate, 0 AS AdClipsProcessed, 0 AS ValidCWT, 0 AS CWTSampleExpansion FROM tempRMIS_EscPrelim LEFT JOIN tempRMISReleasePrelim ON tempRMIS_EscPrelim.CWTcd = tempRMISReleasePrelim.CWT_codes GROUP BY tempRMIS_EscPrelim.SurveyYear, tempRMIS_EscPrelim.SurveyMetaID, tempRMIS_EscPrelim.LocationCD, tempRMIS_EscPrelim.Location, tempRMIS_EscPrelim.Condition, [enter escapement from CJS], 0, 0, 0, 0;

'Analysis 6 Update percentages
 
UPDATE DISTINCTROW tempRMIS_EscSummary SET tempRMIS_EscSummary.SampleRate = IIf(tempRMIS_EscSummary!ChinookSampled=0, 0, tempRMIS_EscSummary!ChinookSampled*100/tempRMIS_EscSummary!Escapement), tempRMIS_EscSummary.AdClipsProcessed = IIf(tempRMIS_EscSummary!HeadProcessed=0, 0, tempRMIS_EscSummary!HeadProcessed*100/(tempRMIS_EscSummary![ObservedAd-clips]+tempRMIS_EscSummary![Ad-ClipUnknown])), tempRMIS_EscSummary.ValidCWT = IIf(tempRMIS_EscSummary!ValidCWTs=0, 0, tempRMIS_EscSummary!ValidCWTs*100/tempRMIS_EscSummary!HeadProcessed);

'Analysis 7 Update final expansion

UPDATE DISTINCTROW tempRMIS_EscSummary SET tempRMIS_EscSummary.CWTSampleExpansion = 1/(tempRMIS_EscSummary!SampleRate/100)*(tempRMIS_EscSummary!AdClipsProcessed/100)*(tempRMIS_EscSummary!ValidCWT/100);