'Start fresh tables
'Build a table from individual records with only the first instance of a tagcode added.
'All remaining records are added as first chops.
'This table ensures that tag codes are unique and all individuals may be included.

#####################################################

'Build tempCJS_Selections NEW TABLE added 10/26/2017

'Qry_CJS01_Selections1 builds table selecting the first instance of a tagcode -added 10/26/2017
	
SELECT SurveyMetadata.SurveyMetaID, Min(CarcassIndividual!IndividualID) AS IndividualID, CarcassIndividual!DiscTagApplied AS DiscTag, 1 AS [Capture Codes], CarcassIndividual.SpeciesID INTO tempCJS_Selections
FROM SurveyMetadata INNER JOIN (CarcassSurvey INNER JOIN CarcassIndividual ON (CarcassSurvey.SurveyID = CarcassIndividual.SurveyID) AND (CarcassSurvey.SurveyMetaID = CarcassIndividual.SurveyMetaID)) ON SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID
GROUP BY SurveyMetadata.SurveyMetaID, CarcassIndividual!DiscTagApplied, 1, SurveyMetadata.ActiveSelection, CarcassIndividual.SpeciesID
HAVING (((SurveyMetadata.ActiveSelection)=Yes) AND (([CarcassIndividual]![DiscTagApplied]) Is Not Null) AND ((CarcassIndividual.SpeciesID)="161980"));

'Qry_CJS01_Selections2 appends all other records in the individual table as first chops building a unique tagcode with a concatination of the SurveyMetaID and IndividualIDs -added 10/26/2017

INSERT INTO tempCJS_Selections ( SurveyMetaID, IndividualID, DiscTag, [Capture Codes] )
SELECT SurveyMetadata.SurveyMetaID, CarcassIndividual!IndividualID AS IndividualID, CarcassIndividual!SurveyMetaID & CarcassIndividual!IndividualID AS DiscTag, 2 AS [Capture Codes]
FROM SurveyMetadata INNER JOIN (CarcassSurvey INNER JOIN (CarcassIndividual LEFT JOIN tempCJS_Selections ON (CarcassIndividual.SurveyMetaID = tempCJS_Selections.SurveyMetaID) AND (CarcassIndividual.IndividualID = tempCJS_Selections.IndividualID)) ON (CarcassSurvey.SurveyID = CarcassIndividual.SurveyID) AND (CarcassSurvey.SurveyMetaID = CarcassIndividual.SurveyMetaID)) ON SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID
WHERE (((SurveyMetadata.ActiveSelection)=Yes) AND ((tempCJS_Selections.IndividualID) Is Null) AND ((CarcassIndividual.SpeciesID)="161980"));


'Build Capture History table

'Qry_CJSCaptureHistory_00MakeMarkedTable tempCJS_CaptureHistoryPrelim-updated 10/26/2017

SELECT SurveyMetadata.SurveyMetaID, CarcassSurvey.SurveyWeek, luRun!run & ' ' & luSpecies!SpeciesCode AS Target, luRun!run & ' ' & luSpecies!SpeciesCode AS Fish, CStr(0) AS DiscTag, 0 AS [Capture Codes], 0 AS IndividualID INTO tempCJS_CaptureHistoryPrelim
FROM (luSpecies INNER JOIN (luRun INNER JOIN SurveyMetadata ON luRun.runID = SurveyMetadata.TargetRunID) ON luSpecies.TaxonID = SurveyMetadata.TargetSpeciesID) INNER JOIN CarcassSurvey ON SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID
GROUP BY SurveyMetadata.SurveyMetaID, CarcassSurvey.SurveyWeek, luRun!run & ' ' & luSpecies!SpeciesCode, luRun!run & ' ' & luSpecies!SpeciesCode, CStr(0), 0, 0, SurveyMetadata.ActiveSelection
HAVING (((SurveyMetadata.ActiveSelection)=Yes));

'Qry_CJSCaptureHistory_01AppendIndividuals -updated 10/26/2017

INSERT INTO tempCJS_CaptureHistoryPrelim ( IndividualID, SurveyMetaID, SurveyWeek, Target, Fish, DiscTag, [Capture Codes] )
SELECT CarcassIndividual.IndividualID, SurveyMetadata.SurveyMetaID, CarcassSurvey.SurveyWeek, luRun!run & ' ' & luSpecies!SpeciesCode AS Target, luRun_1!run & ' ' & luSpecies_1!SpeciesCode AS Fish, tempCJS_Selections.DiscTag, tempCJS_Selections.[Capture Codes]
FROM (luSpecies INNER JOIN (luRun INNER JOIN SurveyMetadata ON luRun.runID = SurveyMetadata.TargetRunID) ON luSpecies.TaxonID = SurveyMetadata.TargetSpeciesID) INNER JOIN (CarcassSurvey INNER JOIN (((CarcassIndividual INNER JOIN luSpecies AS luSpecies_1 ON CarcassIndividual.SpeciesID = luSpecies_1.TaxonID) INNER JOIN luRun AS luRun_1 ON CarcassIndividual.RunID = luRun_1.runID) INNER JOIN tempCJS_Selections ON (CarcassIndividual.SurveyMetaID = tempCJS_Selections.SurveyMetaID) AND (CarcassIndividual.IndividualID = tempCJS_Selections.IndividualID)) ON (CarcassSurvey.SurveyID = CarcassIndividual.SurveyID) AND (CarcassSurvey.SurveyMetaID = CarcassIndividual.SurveyMetaID)) ON SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID
WHERE (((SurveyMetadata.ActiveSelection)=Yes));

'Qry_CJSCaptureHistory_02AppendRecaps appends recovered disc tag data to the table above -updated 10/26/2017

INSERT INTO tempCJS_CaptureHistoryPrelim ( SurveyMetaID, SurveyWeek, Target, Fish, DiscTag, [Capture Codes] )
SELECT SurveyMetadata.SurveyMetaID, CarcassSurvey.SurveyWeek, luRun!run & ' ' & luSpecies!SpeciesCode AS Target, luRun_1!run & ' ' & luSpecies_1!SpeciesCode AS Fish, CarcassRecover.TagRecovered AS DiscTag, Max(IIf(CarcassRecover!DispositionID=2,2,1)) AS [Capture Codes]
FROM (luSpecies INNER JOIN (luRun INNER JOIN SurveyMetadata ON luRun.runID = SurveyMetadata.TargetRunID) ON luSpecies.TaxonID = SurveyMetadata.TargetSpeciesID) INNER JOIN (CarcassSurvey INNER JOIN (((CarcassRecover INNER JOIN tempCJS_Selections ON (CarcassRecover.SurveyMetaID = tempCJS_Selections.SurveyMetaID) AND (CarcassRecover.TagRecovered = tempCJS_Selections.DiscTag)) INNER JOIN (luRun AS luRun_1 INNER JOIN CarcassIndividual ON luRun_1.runID = CarcassIndividual.RunID) ON (tempCJS_Selections.SurveyMetaID = CarcassIndividual.SurveyMetaID) AND (tempCJS_Selections.IndividualID = CarcassIndividual.IndividualID)) INNER JOIN luSpecies AS luSpecies_1 ON CarcassIndividual.SpeciesID = luSpecies_1.TaxonID) ON (CarcassSurvey.SurveyID = CarcassRecover.SurveyID) AND (CarcassSurvey.SurveyMetaID = CarcassRecover.SurveyMetaID)) ON SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID
GROUP BY SurveyMetadata.SurveyMetaID, CarcassSurvey.SurveyWeek, luRun!run & ' ' & luSpecies!SpeciesCode, luRun_1!run & ' ' & luSpecies_1!SpeciesCode, CarcassRecover.TagRecovered, SurveyMetadata.ActiveSelection, tempCJS_Selections.[Capture Codes]
HAVING (((CarcassRecover.TagRecovered) Is Not Null) AND ((SurveyMetadata.ActiveSelection)=Yes) AND ((tempCJS_Selections.[Capture Codes])=1));

'Qry_CJSCaptureHistory_04Crosstab (NOT AN ACTION QUERY) this is a crosstab and is used to build final 06 query -updated 11/6/2016

TRANSFORM CInt(IIf(Max([tempCJS_CaptureHistoryPrelim]![Capture Codes]) Is Null,0,Max([tempCJS_CaptureHistoryPrelim]![Capture Codes]))) AS CaptureCodes
SELECT tempCJS_CaptureHistoryPrelim.SurveyMetaID, tempCJS_CaptureHistoryPrelim.Target, tempCJS_CaptureHistoryPrelim.Fish, Str(tempCJS_CaptureHistoryPrelim!DiscTag) AS DiscTag
FROM tempCJS_CaptureHistoryPrelim
WHERE (((tempCJS_CaptureHistoryPrelim.DiscTag)<>"0"))
GROUP BY tempCJS_CaptureHistoryPrelim.SurveyMetaID, tempCJS_CaptureHistoryPrelim.Target, tempCJS_CaptureHistoryPrelim.Fish, Str(tempCJS_CaptureHistoryPrelim!DiscTag)
ORDER BY Str(tempCJS_CaptureHistoryPrelim!DiscTag)
PIVOT tempCJS_CaptureHistoryPrelim.SurveyWeek;

'Qry_CJSCaptureHistory_05Final Makes an export table with the crosstab query above...05 -updated 10/26/2017

SELECT Qry_CJSCaptureHistory_04Crosstab.* INTO tempCJS_CaptureHistory
FROM Qry_CJSCaptureHistory_04Crosstab;

'Build Chops Matrix table

'Qry_CJSChopsMatrix_01Step01MakeChopsTable Builds tempCJS_ChopsMatrixPrelim table -updated 10/26/2017

SELECT SurveyMetadata.SurveyMetaID, luRun!run & ' ' & luSpecies!SpeciesCode AS Target, luRun!run & ' ' & luSpecies!SpeciesCode AS Fish, CarcassSurvey.SurveyWeek, 0 AS Chops INTO tempCJS_ChopsMatrixPrelim
FROM (luSpecies INNER JOIN (luRun INNER JOIN SurveyMetadata ON luRun.runID = SurveyMetadata.TargetRunID) ON luSpecies.TaxonID = SurveyMetadata.TargetSpeciesID) INNER JOIN CarcassSurvey ON SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID
GROUP BY SurveyMetadata.SurveyMetaID, luRun!run & ' ' & luSpecies!SpeciesCode, luRun!run & ' ' & luSpecies!SpeciesCode, CarcassSurvey.SurveyWeek, 0, SurveyMetadata.ActiveSelection
HAVING (((SurveyMetadata.ActiveSelection)=Yes));

'Qry_CJSChopsMatrix_02AppendWeeklyChopTotals Appends sum of chops per week -updated 10/26/2017

INSERT INTO tempCJS_ChopsMatrixPrelim ( SurveyMetaID, SurveyWeek, Target, Fish, Chops )
SELECT SurveyMetadata.SurveyMetaID, CarcassSurvey.SurveyWeek, luRun!run & ' ' & luSpecies!SpeciesCode AS Target, luRun_1!run & ' ' & luSpecies_1!SpeciesCode AS Fish, Sum(CarcassChops.ChopCount) AS Chops
FROM (luSpecies INNER JOIN (luRun INNER JOIN SurveyMetadata ON luRun.runID = SurveyMetadata.TargetRunID) ON luSpecies.TaxonID = SurveyMetadata.TargetSpeciesID) INNER JOIN (CarcassSurvey INNER JOIN ((CarcassChops LEFT JOIN luRun AS luRun_1 ON CarcassChops.RunID = luRun_1.runID) LEFT JOIN luSpecies AS luSpecies_1 ON CarcassChops.SpeciesID = luSpecies_1.TaxonID) ON (CarcassSurvey.SurveyID = CarcassChops.SurveyID) AND (CarcassSurvey.SurveyMetaID = CarcassChops.SurveyMetaID)) ON SurveyMetadata.SurveyMetaID = CarcassSurvey.SurveyMetaID
GROUP BY SurveyMetadata.SurveyMetaID, CarcassSurvey.SurveyWeek, luRun!run & ' ' & luSpecies!SpeciesCode, luRun_1!run & ' ' & luSpecies_1!SpeciesCode, SurveyMetadata.ActiveSelection;

'Qry_CJSChopsMatrix_03Crosstab is a crosstab query (NOT AN ACTION QUERY) stored inside the U.I. and used below to export data  -updated 10/26/2017

TRANSFORM Sum(tempCJS_ChopsMatrixPrelim.Chops) AS SumOfChops
SELECT tempCJS_ChopsMatrixPrelim.SurveyMetaID
FROM tempCJS_ChopsMatrixPrelim
GROUP BY tempCJS_ChopsMatrixPrelim.SurveyMetaID
PIVOT tempCJS_ChopsMatrixPrelim.SurveyWeek;

'Qry_CJSChopsMatrix_04Final makes the final chops table called tempCJS_ChopsMatrix  -updated 10/26/2017
	
SELECT Qry_CJSChopsMatrix_03Crosstab.* INTO tempCJS_ChopsMatrix
FROM Qry_CJSChopsMatrix_03Crosstab;

'Build Covariate table

'Qry_CJSCovariates_01Individuals Builds the covariate table and adds all covariate data -updated 10/26/2017

SELECT tempCJS_Selections.DiscTag, luSex!SexCD AS Sex, CStr(IIf(CarcassIndividual!FLmm Is Null,'N/R',CarcassIndividual!FLmm)) AS Length INTO tempCJS_Covariate
FROM luSex RIGHT JOIN ((tempCJS_CaptureHistoryPrelim INNER JOIN CarcassIndividual ON (tempCJS_CaptureHistoryPrelim.SurveyMetaID = CarcassIndividual.SurveyMetaID) AND (tempCJS_CaptureHistoryPrelim.IndividualID = CarcassIndividual.IndividualID)) INNER JOIN tempCJS_Selections ON (CarcassIndividual.IndividualID = tempCJS_Selections.IndividualID) AND (CarcassIndividual.SurveyMetaID = tempCJS_Selections.SurveyMetaID)) ON luSex.SexID = CarcassIndividual.SexID;